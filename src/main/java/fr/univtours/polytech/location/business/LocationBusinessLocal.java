package fr.univtours.polytech.location.business;

import java.util.List;

import fr.univtours.polytech.location.model.LocationBean;
import jakarta.ejb.Local;

@Local
public interface LocationBusinessLocal {

    public void addLocation(LocationBean locationBean);

    public List<LocationBean> getLocations();

    public LocationBean getLocation(Integer id);

    public void updateLocation(LocationBean locationBean);

    public void deleteLocation(Integer id);
}
