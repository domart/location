package fr.univtours.polytech.location.business;

import java.util.List;

import fr.univtours.polytech.location.model.LocationBean;
import jakarta.ejb.Remote;

@Remote
public interface LocationBusinessRemote {

    public void addLocation(LocationBean bean);

    public List<LocationBean> getLocations();

    public LocationBean getLocation(Integer id);
}
